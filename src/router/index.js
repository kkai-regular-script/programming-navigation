import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);

// 路由懒加载
const Home = () => import("../views/home/Home.vue");

const routes = [
  {
    path: "/",
    redirect: "/home",
  },
  {
    path: "/home",
    component: Home,
  },
];

const router = new VueRouter({
  routes,
  name: "history",
});

const routerPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return routerPush.call(this, location).catch((error) => error);
};

export default router;
