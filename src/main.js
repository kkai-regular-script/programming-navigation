import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Antd from "ant-design-vue/es"; /* 07.12j解决antd为未定义 */
import "ant-design-vue/dist/antd.css";
import "./ant-design/index"; /* 按需导入写法 */
import { Button, message } from "ant-design-vue";
Vue.use(Button);
Vue.use(Antd);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
