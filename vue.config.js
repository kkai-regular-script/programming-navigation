const path = require("path");
module.exports = {
  // entry: "./src/main.js",
  // output: {
  //   path: path.resolve(__dirname, "dist"), //dirname获取的时绝对路径的名字
  //   filename: "dist.bundle.js",
  // },
  // ant-design由于less版本问题
  css: {
    loaderOptions: {
      less: {
        javascriptEnabled: true,
      },
    },
  },
  // module: {
  //   rules: [
  //     {
  //       test: /\.css$/,
  //       // 注意：loaders 是 use 的别名,下面的loaders也可以改成use
  //       use: [
  //         {
  //           loader: "style-loader",
  //         },
  //         {
  //           loader: "css-loader",
  //         },
  //         {
  //           loader: "less-loader",
  //           options: {
  //             javascriptEnabled: true,
  //           },
  //         },
  //       ],
  //     },
  //   ],
  // },
};
