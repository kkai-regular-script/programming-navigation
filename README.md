# 编程导航
搭建项目，完成度2%，时间2021-07-15

#### 介绍
基于vue + Spring Boot的编程导航项目，灵感来源于程序员鱼皮~，前端ui框架选取ant-desgin-vue

#### 项目记录
1. 07.13-完成了路由配置，通过脚手架搭建vue2项目，引入ant-design框架
2. 07.15 完成了首页的制作，进行下一步路由的配置



#### 软件架构
软件架构说明

#### 效果图


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
