module.exports = {
  presets: ["@vue/cli-plugin-babel/preset"],
  /* 按需导入ant组件 */
  plugins: [
    [
      "import",
      { libraryName: "ant-design-vue", libraryDirectory: "es", style: true },
    ],
  ],
};
